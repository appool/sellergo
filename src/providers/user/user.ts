import { Injectable } from '@angular/core';

@Injectable()
export class UserProvider {

  user: User = {};

  constructor() {
    
  }

  loadUser(name: string, email: string, picture: string, token: string){
    this.user.name = name;
    this.user.email = email;
    this.user.picture = picture;
    this.user.token = token;
  }

}

export interface User {
  type?: number;
  name? : string;
  email? : string;
  picture? : string;
  token? : string;
}
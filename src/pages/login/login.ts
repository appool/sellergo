import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

// Providers
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private afAuth: AngularFireAuth, private userProvider: UserProvider, private fb: Facebook, private platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  createMessage(title, message){
    let alert = this.alertCtrl.create({
      title: title,
      message: message
    });
    alert.present();
  }

  askUserType(){

    return this.alertCtrl.create({
      title: '¿Continuar como vendedor?',
      message: 'De click en \"Sí\", si quieres continuar como vendedor, de lo contrario de click en \"No\"',
      buttons: [
        {
          text: 'No',
          handler: () => {
            this.userProvider.user.type = 0;
          }
        },
        {
          text: 'Sí',
          handler: () => {
            this.userProvider.user.type = 1;
          }
        }
      ]
    });

  }

  signInWithEmail(){
    this.askUserType();
  }

  signInWithGoogle(){
    this.askUserType();    
  }

  signInWithFacebook(){ 

    if(this.platform.is('cordova')){

      let questionType = this.askUserType();
      questionType.present();
      questionType.onDidDismiss(()=>{
        if(this.userProvider.user.type != undefined){
          
          this.fb.login(['email', 'public_profile']).then(res => {
            return firebase.auth().signInWithCredential(firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken))
            .then(user => {
    
              this.userProvider.loadUser(
                user.displayName,
                user.email,
                user.photoURL,
                user.uid,
              );
    
              this.navCtrl.setRoot(HomePage);                   
    
            }).catch(e => console.log('Error con el login'+JSON.stringify(e)));
          });

        }
      });      

    }else{

      let questionType = this.askUserType();
      questionType.present();
      questionType.onDidDismiss(()=>{
        if(this.userProvider.user.type != undefined){
          this.afAuth.auth
          .signInWithPopup(new firebase.auth.FacebookAuthProvider())
          .then(res => {
    
            let user = res.user;
    
            this.userProvider.loadUser(
              user.displayName,
              user.email,
              user.photoURL,
              user.uid,
            );          
    
            this.navCtrl.setRoot(HomePage);
    
          });
        }
      });      

    }

  }

}

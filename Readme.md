# SellerGO 📱
---

## Requisitos para el test 📋
 - Instalar Node.js desde https://nodejs.org/es/ 
 - Instalar Angular desde https://cli.angular.io/
 - Instalar Ionic 4.2 desde el cmd corriendo el comando "npm install -g ionic@4.2"
 ![](https://i.imgur.com/6dB0WC8.png) 

## Pasos para ejecutar el proyecto 📑
 - Clonar este repositorio
 - Entrar a la ruta del proyecto desde el cmd
 - ejecutar el comando "git checkout developer"
 - Luego de estar dentro de la carpeta desde el cmd, 
 ejecutar el comando "ionic serve" y dar sí a todo

## Si tiene dudas contactenos
##### Santiago Quintero - 3124813515 📲